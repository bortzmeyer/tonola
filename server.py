#!/usr/bin/env python3

# Not configurable
MAXTOOT = 10000000
MAXNOTELENGTH = 10000
MAXKEYLENGTH = 130
RFC3339 = '%Y-%m-%dT%H:%M:%SZ'
IMF = '%a, %d %b %Y %H:%M:%S GMT'
__URL__ = 'https://framagit.org/bortzmeyer/tonola'

# Defaults
# At the present time, only one user per instance
# Files and databases
databasename = 'tonola.db'
configfilename = 'config.ini'
dumpfilename = 'dumpexample.json'
# User
username = 'tonola'
password = None
public_key = ''
# Server
robotstxt = """User-agent: *
Disallow: /
"""
user_agent = ''
domainname = ''
logfilename = 'tonola.log'
# Host name, or an empty string to bind to all IP addresses
me = ''
port = 5678
delta = 300 # Time window in seconds (Create with a larger offset will be rejected)
longdelta = 3600 # Time window in seconds for delayed toots 
# Presentation
# Display in the Web interface
max_toots = 50

# State. Most of it is made of ActivityPub collections. And all the
# state is in the SQLite database. We don't keep an in-memory cache.

# Local modules
import signatures

# https://gitlab.com/pgjones/quart
from quart import Quart, request, Response, abort

# http://www.yattag.org/
from yattag import Doc

# https://docs.aiohttp.org/
import aiohttp

# https://pyopenssl.org/
import OpenSSL.crypto

# https://lxml.de/
from lxml.html.clean import Cleaner
from lxml import etree, html

# https://docs.python.org/3/library/sqlite3.html
import sqlite3

import configparser
import sys
import re
import base64
import getopt
import json
import socket
import time
import string
import logging
import random
import urllib.parse
import asyncio
import datetime
import os.path

# Read it from outside with a configuration file
cfg = configparser.ConfigParser()
default_section = 'CONFIG'

class InternalError(Exception):
    pass

def error(msg):
    logger.error(msg)
    
def warning(msg):
    logger.warning(msg)

def info(msg):
    logger.info(msg)

def debug(msg):
    logger.debug(msg)

def usage(msg=None):
    if msg:
        print(msg,file=sys.stderr)
    print("Usage: %s [-q] [-u user] [-p listen-port] [-d domain] [-l logfilename.log] [-c cfgfilename.ini] [-D (or 'db') databasename.db]" % sys.argv[0], file=sys.stderr)

def curtime():
    return time.strftime(RFC3339, time.gmtime(time.time()))

def creationtable():
    # Note: most tables use also the implicit unique ID "rowid".
    curs.execute('''CREATE TABLE IF NOT EXISTS Identifier (id INTEGER, value INTEGER)''')
    curs.execute('''SELECT value FROM Identifier WHERE id=1''')
    curs.execute('''PRAGMA foreign_keys = ON''')
    r = curs.fetchone()
    if r is None:
        curs.execute('''INSERT INTO Identifier (id, value) VALUES (?,?)''', (1, 0))
    # We have a table Toots, but no table Activities. We could have such a
    # table (and a link between the two tables) but we don't try to be a
    # generic ActivityPub server, we are focused on toots.
    curs.execute('''CREATE TABLE IF NOT EXISTS Users (address TEXT UNIQUE, uri TEXT UNIQUE NOT NULL PRIMARY KEY, username TEXT, displayname TEXT, added TEXT, inbox TEXT, sharedinbox TEXT)''')
    # "sender" is an URI, an entry into table Users
    curs.execute('''CREATE TABLE IF NOT EXISTS Toots (id INTEGER PRIMARY KEY, uri TEXT UNIQUE NOT NULL, content TEXT, type TEXT, rawactivity TEXT, sender TEXT, date TEXT, boost BOOLEAN, private BOOLEAN, FOREIGN KEY(sender) REFERENCES Users(uri) ON DELETE CASCADE)''') 
    # Adding a hash of the toot would allow to detect easily
    # duplicated toots. But that's touchy because collisions may
    # happen. (A message "Hello, everybody")
    curs.execute('''CREATE TABLE IF NOT EXISTS Keys (keyid TEXT UNIQUE PRIMARY KEY, key TEXT UNIQUE, last_checked TEXT)''')
    # "user" and "key" are URIs
    curs.execute('''CREATE TABLE IF NOT EXISTS UsersKeys (user TEXT, key TEXT, FOREIGN KEY(user) REFERENCES Users(uri) ON DELETE CASCADE, FOREIGN KEY(key) REFERENCES Keys(keyid) ON DELETE CASCADE)''')
    curs.execute('''CREATE TABLE IF NOT EXISTS Peers (name TEXT UNIQUE PRIMARY KEY, date TEXT)''')
    curs.execute('''CREATE TABLE IF NOT EXISTS Following (address TEXT UNIQUE PRIMARY KEY, uri TEXT UNIQUE, key TEXT UNIQUE, state TEXT, date TEXT)''')
    curs.execute('''CREATE TABLE IF NOT EXISTS Followers (uri TEXT UNIQUE PRIMARY KEY, date TEXT)''')
    bdd.commit()

def check():
    wrong = False
    if public_key == '':
       usage('no public key found')
       wrong = True
    elif robotstxt == '':
       usage('no robotstxt found')
       wrong = True
    elif username == '':
       usage('no username found')
       wrong = True
    elif port == None:
       usage('no port found')
       wrong = True
    elif domainname == '':
       usage('no domain name found')
       wrong = True
    elif databasename == '':
       usage('no database name found')
       wrong = True
    elif logfilename == '':
       usage('no logfile name found')
       wrong = True
    elif __name__ == '':
       usage('no __name__ found')
       wrong = True
    elif __version__ == '':
       usage('no __version__ found')
       wrong = True
    if wrong == True:
       sys.exit(1)

# The ActivityStreams standard say that content is, by default, HTML. https://www.w3.org/TR/activitystreams-vocabulary/#dfn-content
def addtoot(id, uri, content, raw, date, sender, type='text/html', boost=False, private=False):
    curs.execute('''SELECT id,date FROM Toots WHERE uri=?''', (uri,))
    r = curs.fetchone()
    if r is not None:
        debug("Toot %s already seen (ID %s) at %s" % (uri, r[0],  r[1]))
    else:
        debug("Add toot from %s" % sender)
        curs.execute('''INSERT INTO Toots (id, uri, content, rawactivity, date, sender, type, boost, private)  VALUES(?,?,?,?,?,?,?,?,?)''',
                         (id, uri, content, raw, date, sender, type, boost, private))
        bdd.commit()

def deltoot(uri, sender):
    # TODO we should test that the toot is written by this sender, to produce a better error message
    curs.execute('''DELETE FROM Toots WHERE uri=? AND sender=?''', (uri,sender))
    bdd.commit()
            
# You can call it on an already existing user, it will be a noop (may be with an update of the display name).
def adduser(address, uri, name, displayname):
    curs.execute('''SELECT username, displayname FROM Users WHERE uri=?''', (uri,))
    r = curs.fetchone()
    if r is not None: 
        if r[1] != displayname:
            debug("Updating display name for %s" % uri)
            curs.execute('''UPDATE Users SET displayname=? WHERE uri=?''', (displayname, uri))
        else:
            debug("User %s already exists, no change" % uri)
            return # User already exists, no change, this is normal
    else:
        debug("Adding user %s" % uri)
        curs.execute('''INSERT INTO Users (address, uri, username, displayname, added) VALUES (?, ?, ?, ?, ?)''',
                         (address, uri, name, displayname, curtime()))
    bdd.commit()

def addkey(keyid,key,user):
    curs.execute('''SELECT key FROM Keys WHERE keyid=?''', (keyid,))
    r = curs.fetchone()
    if r is not None:
        if r[0] != key:
            debug("Updating key %s" % key)
            curs.execute('''UPDATE Keys SET key=?, user=?, last_checked WHERE keyid=?''', (key, keyid, user, curtime()))
        else:
            return # Key already exists
    debug("Adding key %s" % keyid)
    curs.execute('''INSERT INTO Keys (keyid,key,last_checked) VALUES (?,?,?)''',(keyid,key,curtime()))
    curs.execute('''INSERT INTO UsersKeys (user, key) VALUES (?, ?)''', (user, keyid))
    bdd.commit()

def adduserofkey(keyid, user):
    curs.execute('''INSERT INTO UsersKeys (user, key) VALUES (?, ?)''', (user, keyid))
    bdd.commit()
    
# Return a new id
def lastid():
    curs.execute('''SELECT value FROM Identifier WHERE id=1''')
    r = curs.fetchone()
    last = int(r[0]) + 1
    curs.execute('''UPDATE Identifier SET value=? WHERE id=1''', (last,))
    bdd.commit()
    return(last)

# Return the decoded toot from the database with the id 'fetchid'
def fetchtoot(fetchid):
    fetchid = int(fetchid) # Just in case we receive a string
    curs.execute('''SELECT content, date, sender, type, boost, private FROM Toots WHERE id = ?''', (fetchid,))
    return curs.fetchone()

def fetchuser(uri):
    curs.execute('''SELECT address, username, displayname FROM Users WHERE uri = ?''', (uri,))
    return curs.fetchone()

# Same as previous but returns the object in JSON
def fetchfullactivity(toot_id):
         fetchid = int(toot_id) # Just in case we receive a string
         curs.execute("SELECT rawactivity FROM Toots WHERE id = ?", (fetchid,))
         get_from_sql=curs.fetchone()
         if get_from_sql != None:
           ftch = json.loads(get_from_sql[0])
           jt = json.dumps(ftch)
         else:
             jt = None
         return(jt)

#write in json format 'toot' in a new 'dumpfilename' 
def dumptoot(dumpfilename,toot):
    with open(dumpfilename,'w+') as f:
         f.write(json.dumps(toot,ensure_ascii = False,indent = 4,sort_keys = True))
         return("toot written on %s" % dumpfilename)

def fetchkey(keyID):
    curs.execute("SELECT key FROM Keys WHERE keyid = ?",(keyID,))
    get_from_sql = curs.fetchone()
    if get_from_sql is None:
        return (None, None)
    else:
        key = get_from_sql[0]
        curs.execute("SELECT user FROM UsersKeys WHERE key = ?",(keyID,))
        get_from_sql = curs.fetchone()
        if get_from_sql is None:
            user = None
        else:
            user = get_from_sql[0]   
        return (key, user)

def updatekey(keyID):
    curs.execute('''UPDATE Keys SET last_checked = ? WHERE keyid = ?''',(curtime(),keyID))
    bdd.commit()

def fetchfollowinguri(uri):
    curs.execute("SELECT Uri FROM Following WHERE uri = ?",(uri,))
    get_from_sql = curs.fetchone()
    if get_from_sql != None:
       return(get_from_sql[0])
    else:
       return(None) 

def reply(tuple):
    # TODO responses in JSON and HTML
    response = ""
    for tuple in curs.fetchall():
        response += str(tuple) + "\n\n"
    return (response)

def origin(request):
    orig = request.headers.get('forwarded')
    if orig is None:
        orig = request.headers.get('x-forwarded-for') # The Apache frontend
        # still uses the old header, not the RFC 7239 one :-(
        if orig is None:
            orig = request.headers.get('remote-addr')
    return orig

def check_date(date, published, now):
    # TODO it seems most (all?) ActivityPub servers don't
    # check the "published" member, only the HTTP Date:
    # header?
    if date is not None:
        debug("Comparing date (%s) and published (%s) and current time (%s)" % (date, published, now))
        if date < published or (published + datetime.timedelta(seconds=longdelta)) < date:
            # Note that it means that we reject old toots when we resume after a long stop. Problem? May be we should keep only the next test?
            debug("Date and published are too different")
            return (False, Response("Date (%s) and Published (%s) too different" % (date, published), status=401, mimetype='text/plain'))
        if date + datetime.timedelta(seconds=delta) < now:
            debug("Date is too old (replay attack, or extreme slowness?)")
            return (False, Response("Date (%s) too old" % (date), status=401, mimetype='text/plain'))
    else:
        # Reject such messages? Apparently, Pleroma sends Follows without Date https://git.pleroma.social/pleroma/pleroma/issues/364 https://github.com/Chocobozzz/PeerTube/issues/1383 Fixed now?
        warning("No Date field in the HTTP request from %s" % orig)
    return (True, None)

# All outgoing HTTP requests must go there
async def urlmethod(url, headers, data=None):
    headers['User-Agent'] = user_agent
    if debug:
        trace_config = aiohttp.TraceConfig()
        trace_config.on_request_start.append(on_request_start)
    # TODO keep the session persistent instead of one per request?
    async with aiohttp.ClientSession(headers=headers, trace_configs=[trace_config]) as session:
        if data:
            request = session.post
        else:
            request = session.get
        try:
            async with request(url, headers=headers, data=data) as response:
                r = await response.text()
                return str(response.status), r
        except aiohttp.client_exceptions.ClientConnectorError as e:
            return "000", "Connection error: %s" % e
        except socket.gaierror:
            return "001", "GAI error"
        
async def urlget(url, headers, data=None):    
    if data is not None:
        raise InternalError("Data must be None for GET requests")
    return await urlmethod(url, headers)
    
async def urlpost(url, headers, data):
    return await urlmethod(url, headers, data)

async def on_request_start(session, trace_config_ctx, params):
    debug("Starting %s request for %s. I will send: %s" % (params.method, params.url, params.headers))

app = Quart(__name__)

@app.route('/', methods=['HEAD', 'GET', 'POST'])
async def index():
    # TODO display something useful. An explanation of this instance,
    # for instance.
    # TODO a link vers a (password-protected) page
    # showing the inbox content in HTML (timeline)
   if request.method == 'POST':
      ct = request.headers.get('content-type')
      data = await request.get_data()
      r = bytes(data)
      debug("Received %s bytes of type %s" % (len(r), ct))
   elif request.method == 'HEAD' or request.method == 'GET':
       doc, tag, text = Doc().tagtext()
       # TODO factorize the head of the page
       doc.asis('<!DOCTYPE html>\n')                                                
       with tag('html'):                                       
           with tag('head'):                                   
               with tag('title'):
                   text("Experimental ActivityPub server %s" % (domainname))
           with tag('body'):
               with tag('h1'):
                   text("Experimental ActivityPub server %s" % (domainname))
               with tag('div'):
                   with tag('ul'):
                       with tag('li'):
                           with tag('a', href = "/toots"):
                               text("List of toots")
                       with tag('li'):
                           with tag('a', href = "/peers"):
                               text("List of peers")
               with tag('hr'):
                   with tag('div'):
                       with tag('p'):
                           text("The other requests require authentication")
                           with tag('ul'):
                               with tag('li'):
                                   with tag('a', href="/tootsprivate"):
                                       text("List of toots, including private ones")
                               with tag('li'):
                                   with tag('a', href = "/knownusers"):
                                       text("List of users")
                               with tag('li'):
                                   with tag('a', href = "/keys"):
                                       text("List of keys")
                               with tag('li'):
                                   with tag('a', href = "/following"):
                                       text("List of following")
                               with tag('li'):
                                   with tag('a', href = "/followers"):
                                       text("List of followers")
       doc.asis(footer)
       return Response(doc.getvalue(), status=200, mimetype='text/html')
   else: # Should not happen
      abort(405)
   try:
       pass
   except:
         error("exception %s" % sys.exc_info()[0])
         return Response("Something wrong", status=500, mimetype='text/plain')
   return Response("OK nothing done (this is a ActivityPub server, not a Web site)", status=200, mimetype='text/plain')

@app.route('/keys', methods=['HEAD', 'GET'])
async def keys():
    # TODO display the user, by reading the table UsersKeys
    (authentic, user, resp) = authenticate(request.headers.get('authorization'), mandatory=True)
    if not authentic or user is None:
        return resp
    curs.execute('''SELECT keyid, last_checked FROM Keys ORDER BY rowid DESC''')
    doc, tag, text = Doc().tagtext()                                                             
    doc.asis('<!DOCTYPE html>\n')                     
    with tag('html'):                                       
        with tag('head'):                                                    
            with tag('title'):
                text("Keys at %s" % (domainname))
        with tag('body'):
            with tag('h1'):
                text("Keys at server %s" % (domainname))
        with tag('ol'):
            for tuple in curs.fetchall():
                with tag('li'):
                    with tag('code'):
                        text(tuple[0])
                    text(" (%s)" % tuple[1])
    doc.asis(footer)
    return Response(doc.getvalue(), status=200, mimetype='text/html')

@app.route('/knownusers', methods=['HEAD', 'GET'])
async def knownusers():
    # TODO display the key, by reading the table UsersKeys
    (authentic, user, resp) = authenticate(request.headers.get('authorization'), mandatory=True)
    if not authentic or user is None:
        return resp
    curs.execute('''SELECT address, uri, username, displayname, added FROM Users ORDER BY rowid DESC''')
    doc, tag, text = Doc().tagtext()                                                             
    doc.asis('<!DOCTYPE html>\n')                     
    with tag('html'):                                       
        with tag('head'):                                                    
            with tag('title'):
                text("Users known at %s" % (domainname))
        with tag('body'):
            with tag('h1'):
                text("Users known at server %s" % (domainname))
        with tag('ol'):
            for tuple in curs.fetchall():
                with tag('li'):
                    # TODO Unicode bug: raw UTF-8 is displayed
                    text("%s / %s " % (tuple[2], tuple[3]))
                    if tuple[0] is not None:
                        with tag('code'):
                            text(tuple[0])
                        text(" ")
                    with tag('code'):
                        text(tuple[1])
                    text(" (%s)" % tuple[4])
    doc.asis(footer)
    return Response(doc.getvalue(), status=200, mimetype='text/html')

@app.route('/.well-known/webfinger', methods=['HEAD', 'GET'])
async def webfinger():
    form = request.args
    orig = origin(request)
    try:
        resource = form['resource']
        info("Webfinger info about %s for %s" % (resource, orig))
        result = re.match('^acct:%s@%s$' % (username, domainname), resource)
        if not result:
            return Response("Unknown account %s at %s" % (resource, domainname), status=410, mimetype='text/plain')
    except KeyError:
        return Response("Invalid Webfinger request", status=400, mimetype='text/plain')
    return Response(json.dumps(webfinger_answer), status=200, mimetype='application/jrd+json')

# TODO implement /.well-known/nodeinfo http://nodeinfo.diaspora.software/protocol.html

@app.route('/robots.txt', methods=['HEAD', 'GET'])
async def robots():
    orig = origin(request)
    info("robots.txt info for %s" % (orig))
    return Response(robotstxt, status=200, mimetype='text/plain')

# TODO implement HEAD methodes differently from GET

@app.route('/actors/<user>', methods=['HEAD', 'GET'])
async def actor(user):
    if user != username:
        return Response("No user named %s here" % user, status=410, mimetype='text/plain')
    form = request.args
    orig = origin(request)
    info("Actor info about %s for %s" % (form, orig))
    return Response(json.dumps(actor_answer), status=200, mimetype='application/jrd+json')

# TODO write.as apparently performs GET on /inbox??? It gets a 405
# then it returns {"code":500,"error_msg":"Couldn't parse actor."})
# TODO what is Mastodon returning when 'GET /inbox'? And what does the
# standard say?
@app.route('/inbox', methods=['POST'])
async def inbox():
    orig = origin(request)
    sig = request.headers.get('signature')
    sig_fields = {}
    if not sig:
        return Response("No signature", status=401, mimetype='text/plain')
    for k, v, terminator in re.findall(r'([a-zA-Z]+)=\"(.+?)\"(,|$)', sig):
        sig_fields[k] = v
    key_id = sig_fields['keyId']
    (rkey, remote_actor) = fetchkey(key_id) 
    if rkey is not None and remote_actor is not None:
        # TODO when a remote instance forwards a toot to the followers
        # (including me), the activity is signed by this relaying
        # instance (ActivityPub is not end-to-end). So, I never need
        # the key of the real sender, and I don't add it in the table
        # Users, which is bad. Use the attached signature, not the
        # HTTP one?
        pass # TODO ability to rekey from time to time? They key may
             # have changed? If key too old, refetch and call updatekey(key_id) ?
    else:
        scheme, machine, path, parameters, \
            query, fragment= \
                urllib.parse.urlparse (key_id)
        (header, date, digest) = signatures.sign(key, my_key,
                                                 {'Host':  machine}, "post /inbox") 
        # TODO do we really need to sign this one?
        status, actor = await urlget(key_id, headers={'Accept': 'application/activity+json', 'Signature': header})
        if status[0] != '2':
            # 410 are normal, with Mastodon sending Delete requests to any instance it knows, when an account is deleted. TODO make it a special case. TODO check the JSON first, that it is indeed a Delete?
            error("Cannot reach %s for key (code %s), may be a Delete from Mastodon for an already deleted user" % (key_id, status))
            return Response("Cannot reach %s for verification (%s)" % (key_id, actor), status=502, mimetype='text/plain')
        try:
            actor_fields = json.loads(actor)
        except JSONerror:
            error("Cannot parse JSON from %s" % (key_id))
            return Response("Cannot parse JSON from %s for verification" % key_id, status=502, mimetype='text/plain')
        rkey = actor_fields['publicKey']['publicKeyPem']
        addkey(key_id, rkey, remote_actor)
        if remote_actor is None:
            adduser(None, actor_fields['id'], actor_fields['preferredUsername'], actor_fields['name'])
            remote_actor = actor_fields['id']
        elif remote_actor == actor_fields['id']:
            adduserofkey(key_id, actor_fields['id'])
    info("Inbox post from %s, testing sig of %s, key is %s, user is %s" % (orig, key_id, rkey[0:MAXKEYLENGTH], remote_actor))
    remote_key = OpenSSL.crypto.load_publickey(OpenSSL.crypto.FILETYPE_PEM, rkey)
    ct = await request.get_data()
    # Should we reject POSTs without a Digest: header? If so, we need to modify the signatures module.
    verif = signatures.verify(public_key=remote_key, signature=sig, headers_values=request.headers,
                              target="post /inbox", body=ct.decode())
    if not verif:                                             
        error("Wrong sig \"%s\" of %s" % (sig, key_id))
        return Response("Wrong signature", status=401, mimetype='text/plain')
    # TODO more checks:
    # * Check of the date for all activities, at least those with a 'published' member
    # * While this proves the request comes from an actor, what if the payload contains an attribution to someone else? In reality you’d want to check that both are the same, otherwise one actor could forge messages from other people. Currenetly, we just log it. (Done nfor Create, do it for others)
    ctj = json.loads(ct.decode())
    dateb = request.headers.get('date')
    if dateb is not None:
        date = datetime.datetime.strptime(dateb, IMF)
    else:
        date = None
    info("Activity of type %s from %s/%s <%s>" % (ctj['type'],  orig, ctj['actor'], ctj['id']))
    debug("Activity received on the general inbox: \"%s\"" % ct.decode())
    # TODO also logs the HTTP headers Link sent by the peer, because
    # the context of the JSON object may depend on it.
    t = time.gmtime(time.time())
    # Switch depending on the activity type https://www.w3.org/TR/activitypub/#client-to-server-interactions
    if ctj['type'] == "Create":
        # TODO check the type of the ctj['object'] first? It is not
        # always a Note (it can be a Mention, for
        # instance, or a video). https://www.w3.org/TR/activitystreams-vocabulary/#object-types
        info("Create activity to %s, cc %s" % (ctj['to'], ctj['cc']))
        if my_id not in ctj['to'] and my_id not in ctj['cc'] and \
           'https://www.w3.org/ns/activitystreams#Public' not in ctj['to'] and 'https://www.w3.org/ns/activitystreams#Public' not in ctj['cc']:
            return Response("This activity does not seem to be for me", status=401, mimetype='text/plain')
        published = datetime.datetime.strptime(re.sub("\.[0-9]+Z$", "Z", ctj['published']), RFC3339)
        now = datetime.datetime.today()
        (success, r) = check_date(date, published, now)
        if not success:
            return r
        json_full = (json.dumps(ctj)).encode()
        json_id = ctj['object']['id']
        json_content = ctj['object']['content']
        if 'https://www.w3.org/ns/activitystreams#Public' in ctj['to'] or 'https://www.w3.org/ns/activitystreams#Public' in ctj['cc']:
            private = False
        else:
            private = True
        # TODO add the type
        if ctj['actor'] != remote_actor: # TODO this breaks when a message is forwarded because one user I follow forwards messages
            error("Signing actor %s is not the same as actor %s in the activity" % (remote_actor, ctj['actor']))
            return Response("Forwarding not yet supported", status=401, mimetype='text/plain') # TODO
        addtoot(lastid(),json_id,json_content,json_full,ctj['published'], ctj['actor'], private=private) # TODO we should check, above, the syntax of published
        # TODO if to or cc is for a collection *we* control, we must forward the activity to the recipients https://www.w3.org/TR/activitypub/#inbox-forwarding
    elif ctj['type'] == "Update":
        pass # Currently, we ignore Update. Never seen from Mastodon
             # but could be fun to implement to see if it works with
             # other software
             # https://www.w3.org/TR/activitystreams-vocabulary/#dfn-update. TODO:
             # check Mastodon's source code.
    elif ctj['type'] == "Delete":
        if ctj['object']['type'] == "Tombstone":
            deltoot(ctj['object']['id'], ctj['actor'])
        else:
            pass
            # Delete can be for an user (then checks it comes from
            # *this* users, and delete its key and the user, and
            # delete all its toots (can be done with CASCADE)
    elif ctj['type'] == "Follow":
       document = accept_message
       document['object']['id'] = ctj['id']
       document['object']['actor'] = ctj['actor']
       #document['signature']['created'] = time.strftime('%Y-%m-%dT%H:%M:%SZ', t)
       #document['signature']['signatureValue'] = 'TODO'
       if 'signature' in document:
           del document['signature'] # TODO reenable when we will have
       # correct sigs The "signature" field is part of
       # https://w3id.org/security/v1 and documented in
       # https://web-payments.org/vocabs/security#signature TODO: be
       # careful, it seems it is necessary to canonicalize the JSON
       # before signing. Can Python do it? Or keep only HTTP signatures and not implement LinkedData signatures (which are mostly for forwarding)?
       verif = signatures.verify(public_key=remote_key, signature=sig, headers_values=request.headers,
                                 target="post /inbox", body=ct.decode())
       if not verif:                                             
            error("Wrong sig \"%s\" of %s for \"%s\": %s" % (sig, key_id, cmp_str, e))
            return Response("Wrong signature: %s" % e, status=401, mimetype='text/plain')
       # TODO we should retrieve the inbox of the user
       scheme, machine, path, parameters, \
           query, fragment= \
               urllib.parse.urlparse (ctj['actor'])
       # Ability to send a Reject activity if we refuse this subscription?
       curs.execute('''SELECT * FROM Peers WHERE name = ?''', (machine, ))
       result = curs.fetchone()
       if not result: # TODO Wait until after our Accept was accepted before registering the peer?
           debug("Adding %s as a federated peer" % machine)
           curs.execute('''INSERT INTO Peers (name, date) VALUES (?, ?)''', (machine, curtime()))
           bdd.commit()
       instance_url = "https://%s/inbox" % machine # TODO uses its inbox. This may be why we sometimes get errors 500
       curs.execute('''SELECT * FROM Followers WHERE uri = ?''', (ctj['actor'], ))
       result = curs.fetchone()
       if result is None:
           curs.execute('''INSERT INTO Followers (uri, date) VALUES (?,?)''',
                        (ctj['actor'], curtime()))
       else:
           pass # TODO a different message if they already follow us?
       bdd.commit()
       jdocument = json.dumps(document)
       (header, date, digest) = signatures.sign(key, my_key,
                                                 {'Host':  machine}, "post /inbox", jdocument)
       status, response = await urlpost(instance_url, headers={'Date': date, 'Signature': header, 'Digest': digest},
                                     data=jdocument.encode())
       debug("Accept message %s sent to %s, response is code %s (%s)" % (jdocument, ctj['actor'], status, response))
    elif ctj['type'] == "Add":
        pass # TODO same thing as Create? Never seen in the wild. TODO check Mastodon's source code
    elif ctj['type'] == "Remove":
        pass # TODO same thing as Delete? Never seen in the wild. TODO check Mastodon's source code
    elif ctj['type'] == "Like":
        # We don't care
        pass
    elif ctj['type'] == "Block":
        # We don't care
        pass
    elif ctj['type'] == "Undo":
        # TODO cancel a follower, remove it from the collection TODO
        # checks that it comes from the *same* actor
        pass
    # Other activities types, listed here, are only used in the
    # server-to-server protocol of ActivityPub. May be we should do
    # the opposite, list the server-to-server activities only since we
    # don't implement the client-to-server part.
    elif ctj['type'] == "Accept":
        actor = ctj['actor']
        curs.execute('''SELECT address,uri,state FROM Following WHERE uri=?''', (actor,))
        rt = curs.fetchone()
        if rt is None:
            return Response("Accept of unknown actor %s ignored" % actor, status=200, mimetype='text/plain')
        elif rt[2] != 'PENDING':
            return Response("Actor %s already accepted" % actor, status=200, mimetype='text/plain')
        else:
            # TODO checks that it comes from the *same* actor
            curs.execute('''UPDATE Following SET state='OK' WHERE uri=?''', (actor,))
            bdd.commit()
            info("%s accepted us" % actor)
            return Response("Actor %s accepts" % actor, status=200, mimetype='text/plain')
    elif ctj['type'] == "Reject":
        actor = ctj['actor']
        curs.execute('''SELECT address,uri,state FROM Following WHERE uri=?''', (actor,))
        rt = curs.fetchone()
        if rt is None:
            return Response("Rejection of unknown actor %s ignored" % actor, status=200, mimetype='text/plain')
        else:
            curs.execute('''DELETE FROM Following WHERE uri=?''', (actor,))
            bdd.commit()
            info("%s rejected us" % actor)
            return Response("Rejection of actor %s duly noted" % actor, status=200, mimetype='text/plain')
    elif ctj['type'] == "Announce":
        # Mastodon use it for boosts. The
        # ActivityPub standard suggests having a "shares" collection,
        # specifically for boosts but we don't follow it yet.
        info("Announce activity (boost) to %s, cc %s" % (ctj['to'], ctj['cc']))
        if my_id not in ctj['to'] and my_id not in ctj['cc'] and \
           'https://www.w3.org/ns/activitystreams#Public' not in ctj['to'] and 'https://www.w3.org/ns/activitystreams#Public' not in ctj['cc']:
            return Response("This activity does not seem to be for me", status=401, mimetype='text/plain')
        published = datetime.datetime.strptime(re.sub("\.[0-9]+Z$", "Z", ctj['published']), RFC3339)
        now = datetime.datetime.today()
        (success, r) = check_date(date, published, now)
        if not success:
            return r
        json_full = (json.dumps(ctj)).encode()
        json_id = ctj['id']
        json_content = ctj['object']
        # TODO add the type
        addtoot(lastid(),json_id,json_content,json_full,ctj['published'], ctj['actor'], boost=True) # TODO we should check, above, the syntax of published 
    else:  # A few types listed
           # https://www.w3.org/TR/activitystreams-vocabulary/#activity-types
           # will fall here.
       warning("Unknown activity type %s, ignoring" % ctj['type'])
    # TODO 200 rcode even if we ignore the activity?
    return Response("Added to INBOX", status=200, mimetype='text/plain')

@app.route('/outbox', methods=['HEAD', 'GET', 'POST'])
async def outbox():
    return Response("No access to the outbox, you naughty", status=403, mimetype='text/plain')

@app.route('/<user>/followers', methods=['HEAD', 'GET', 'POST'])
async def followers(user):
    return Response("No access to the followers of %s, you naughty" % user, status=403, mimetype='text/plain')

@app.route('/</user>/following', methods=['HEAD', 'GET', 'POST'])
async def following(user):
    return Response("No access to the following of %s, you naughty" % user, status=403, mimetype='text/plain')

@app.route('/</user>/liked', methods=['HEAD', 'GET', 'POST'])
async def liked(user):
    return Response("No access to the liked of %s, you naughty" % user, status=403, mimetype='text/plain')

# TODO implement a way to get info about the user

def clean_html(html):
    # Mastodon uses sanitize https://github.com/rgrove/sanitize.
    # See
    # https://github.com/tootsuite/mastodon/pull/10629#issuecomment-485927096
    # for an interesting discussion on these format issues.
    # Remember that toots can be in any format
    # <https://www.w3.org/TR/activitystreams-vocabulary/#dfn-content>
    # even if Mastodon assumes HTML by default
    return html_cleaner.clean_html(html)
    
# Dynamic routing in Quart is documented in https://pgjones.gitlab.io/quart/routing.html

@app.route('/toots/<toot_id>', methods=['HEAD', 'GET'])
async def get_toot(toot_id):
    tuple = fetchtoot(toot_id)
    if tuple is None:
        return Response("No such toot %s" % toot_id, status=404, mimetype='text/plain')
    toot = tuple[0]
    date = tuple[1]
    sender = tuple[2]
    type = tuple[3]
    boost = tuple[4] == 1
    private = tuple[5] == 1
    if private:
        (authentic, user, resp) = authenticate(request.headers.get('authorization'), mandatory=True)
        if not authentic or user is None:
            abort(403)
    tuple = fetchuser(sender)
    if tuple is not None: # TODO should not happen but adduser is not always called
        address = tuple[0]
        name = tuple[1]
        displayname = tuple[2]
    else:
        name = None
    requested = request.headers.get('accept')
    # TODO accept other types? Images, for instance?
    sent = request.accept_mimetypes.best_match(['application/json', 'text/html']) # Bug in https://gitlab.com/pgjones/quart/issues/188
    if not sent:
        return Response("No MIME type found for your Accept (%s)\n" % requested, status=406, mimetype='text/plain')
    # TODO different result depending on sent. Also, may be test
    # quality (browsers send */* but prefer HTML? See
    # http://flask.pocoo.org/snippets/45/ but bug in Quart see
    # https://gitlab.com/pgjones/quart/issues/189
    debug("Fetching %s to %s" % (toot_id, sent))
    # TODO display picture if there is one attached
    if sent == 'application/json':
        toot = fetchfullactivity(toot_id)
        return Response(toot, status=200, mimetype='application/json')
    elif sent == 'text/html':
        doc, tag, text = Doc().tagtext()
        if type != 'text/html':
            return Response("Toot not in HTML", status=406, mimetype='text/plain')
        doc.asis('<!DOCTYPE html>\n')                                                     
        with tag('html'):                                       
            with tag('head'):                               
                with tag('title'):
                    text("Toot #%s on %s" % (toot_id, domainname))
            with tag('body'):
                with tag('h1'):
                    text("Toot #%s on " % (toot_id))
                    with tag('a', href = "/"):
                         text(domainname)
                with tag('div'):
                    with tag('p'):
                        text("Sent by ")
                        if name is not None:                        
                            text(displayname)
                            text(" (")
                            text(name)
                            text(") ")
                            if address is not None:
                                with tag('code'):
                                    text(address)
                                    text(" / ")
                        with tag('code'):
                            text(sender)
                        text(" on %s" % (date))
                    if not boost:
                        doc.asis(clean_html(toot)) 
                    else:
                        with tag('code'):
                            with tag('a', href = toot):
                                text(toot)                        
        doc.asis(footer)
        return Response(doc.getvalue(), status=200, mimetype='text/html')
    else:
        raise Exception("Unknown MIME type %s" % sent)

@app.route('/toots', methods=['HEAD', 'GET'])
async def all_toots():
    return get_all_toots(privateAlso = False)

@app.route('/tootsprivate', methods=['HEAD', 'GET'])
async def all_toots_private():
    (authentic, user, resp) = authenticate(request.headers.get('authorization'), mandatory=True)
    if authentic and user is not None:
        return get_all_toots(privateAlso = True)
    else:
        return resp

def get_all_toots(privateAlso = False, max = max_toots):
    if privateAlso:
        restriction_p = ""
    else:
        restriction_p = " WHERE NOT private"
    if max_toots is not None:
        restriction_l = " LIMIT %i" % max_toots
    else:
        restriction_l = ""
    curs.execute('''SELECT id, uri, content, date, boost, type FROM Toots %s ORDER BY date DESC %s''' % (restriction_p, restriction_l))
    doc, tag, text = Doc().tagtext()
    doc.asis('<!DOCTYPE html>\n')                   
    with tag('html'):                                       
        with tag('head'):                                                          
            with tag('title'):
                text("Toots at %s" % (domainname))
        with tag('body'):
            with tag('h1'):
                text("Toots at server %s" % (domainname))
        with tag('ol'):
            for tuple in curs.fetchall():
                boost = tuple[4] == 1
                type = tuple[5]
                with tag('li'):
                    with tag('a', href = "/toots/%s" % tuple[0]):
                        text(tuple[0])
                    text(" ")
                    with tag('code'):
                        with tag('a', href = tuple[1]):
                            text(tuple[1])
                        text(" ")
                    if not boost:
                        if type != 'text/html':
                            text("Toot not in HTML")
                        else:
                            doc.asis(clean_html(tuple[2]))
                    else:
                        with tag('code'):
                            with tag('a', href = tuple[2]):
                                text(tuple[2])                        
                    text(" (%s)" % tuple[3])
                    with tag('br'):
                        pass
    doc.asis(footer)
    return Response(doc.getvalue(), status=200, mimetype='text/html')

@app.route('/activities/<activity_id>', methods=['HEAD', 'GET'])
async def get_activity(activity_id):
    tuple = fetchtoot(activity_id)
    if tuple is None:
        return Response("No such toot %s" % activity_id, status=404, mimetype='text/plain')
    toot = tuple[0]
    private = tuple[4] == 1
    if private:
        (authentic, user, resp) = authenticate(request.headers.get('authorization'), mandatory=True)
        if not authentic or user is None:
            abort(403)
    toot = fetchfullactivity(activity_id)
    return Response(toot, status=200, mimetype='application/json')

@app.route('/peers', methods=['HEAD', 'GET'])
async def all_peers():
    curs.execute('''SELECT * FROM Peers ORDER by date DESC''')
    doc, tag, text = Doc().tagtext()
    doc.asis('<!DOCTYPE html>\n')                          
    with tag('html'):                                       
        with tag('head'):                                       
            with tag('title'):
                text("Peers of %s" % (domainname))
        with tag('body'):
            with tag('h1'):
                text("Peers of server %s" % (domainname))
        with tag('ol'):
            for tuple in curs.fetchall():
                with tag('li'):
                    with tag('a', href = "https://%s/" % tuple[0]):
                        text(tuple[0])
                    text(" ")
                    text(" (%s)" % tuple[1])
                    with tag('br'):
                        pass
    doc.asis(footer)
    return Response(doc.getvalue(), status=200, mimetype='text/html')

def authenticate(auth, mandatory=True):
    """auth must be the entire content of HTTP header Authorization. RFC
    7617. Returns a triple, a boolean indicating success, an optional
    user name if authenticated, and an optional Response.
    """
    if password is None:
        return (False, None, r)
    if auth is not None and auth.startswith('Basic '):
        (basic, authcontent) = auth.split(' ')
        kv = base64.b64decode(authcontent).decode()
        (user, pwd) = kv.split(':')
        if user == username and pwd == password:
            debug("Access granted for user \"%s\"" % user)
            return (True, user, None)
        else:
            debug("Wrong authentication for user \"%s\"" % (user))
    if mandatory:
        r = Response("You need to authenticate", status=401, mimetype='text/plain')
        r.headers.add(key='WWW-Authenticate', value='Basic realm="TONOLA"')
        return (False, None, r)
    else:
        return (True, None, None)

@app.route('/following', methods=['HEAD', 'GET'])
async def all_following():
    (authentic, user, resp) = authenticate(request.headers.get('authorization'), mandatory=True)
    if not authentic or user is None:
        return resp
    curs.execute('''SELECT address, uri, state, date FROM Following ORDER by date DESC''')
    doc, tag, text = Doc().tagtext()
    doc.asis('<!DOCTYPE html>\n')                                       
    with tag('html'):                                       
        with tag('head'):                             
            with tag('title'):
                text("Following by %s@%s" % (username, domainname))
        with tag('body'):
            with tag('h1'):
                text("Following by %s@%s" % (username, domainname))
        with tag('ol'):
            for tuple in curs.fetchall():
                with tag('li'):
                    with tag('code'):
                        with tag('a', href = tuple[1]):
                            text(tuple[0])
                    text(" ")
                    text(" (%s)" % tuple[2])
                    text(" ")
                    text(" (%s)" % tuple[3])
                    with tag('br'):
                        pass
    doc.asis(footer)
    return Response(doc.getvalue(), status=200, mimetype='text/html')
    
@app.route('/followers', methods=['HEAD', 'GET'])
async def all_followers():
    (authentic, user, resp) = authenticate(request.headers.get('authorization'), mandatory=True)
    if not authentic or user is None:
        return resp
    curs.execute('''SELECT uri, date FROM Followers ORDER by date DESC''')
    doc, tag, text = Doc().tagtext()
    doc.asis('<!DOCTYPE html>\n')                                
    with tag('html'):                                       
        with tag('head'):                                                   
            with tag('title'):
                text("Followers of %s@%s" % (username, domainname))
        with tag('body'):
            with tag('h1'):
                text("Followers of %s@%s" % (username, domainname))
        with tag('ol'):
            for tuple in curs.fetchall():
                with tag('li'):
                    with tag('a', href = tuple[0]):
                        text(tuple[0])
                    text(" ")
                    text(" (%s)" % tuple[1])
                    with tag('br'):
                        pass
    doc.asis(footer)
    return Response(doc.getvalue(), status=200, mimetype='text/html')
    
# The rest is the API

@app.route('/api/custom/inspect/keys', methods=['HEAD', 'GET'])
async def inspect_keys():
    orig = origin(request)
    info("Inspect keys query from %s" % orig)
    curs.execute('''SELECT keyid, last_checked FROM Keys''')
    all_keys = ""
    for tuple in curs.fetchall():
        all_keys += str(tuple)
    return Response(all_keys, status=200, mimetype='text/plain')

@app.route('/api/custom/inspect/following', methods=['HEAD', 'GET'])
async def inspect_following():
    (authentic, user, resp) = authenticate(request.headers.get('authorization'), mandatory=True)
    if not authentic or user is None:
        abort(403)
    orig = origin(request)
    info("Inspect following query from %s" % orig)
    curs.execute('''SELECT * FROM Following''')
    return Response(reply(tuple), status=200, mimetype='text/plain')

@app.route('/api/custom/inspect/followers', methods=['HEAD', 'GET'])
async def inspect_followers():
    (authentic, user, resp) = authenticate(request.headers.get('authorization'), mandatory=True)
    if not authentic or user is None:
        abort(403)
    orig = origin(request)
    info("Inspect followers query from %s" % orig)
    curs.execute('''SELECT * FROM Followers''')
    return Response(reply(tuple), status=200, mimetype='text/plain')

@app.route('/api/custom/inspect/peers', methods=['HEAD', 'GET'])
async def inspect_peers():
    orig = origin(request)
    info("Inspect peers query from %s" % orig)
    curs.execute('''SELECT * FROM Peers''')
    return Response(reply(tuple), status=200, mimetype='text/plain')

async def do_post(data, htmlize=True):
    # htmlize means turning plain text into HTML. With htmlize=False,
    # the content is posted as-is.
    (authentic, user, resp) = authenticate(request.headers.get('authorization'), mandatory=True)
    if not authentic or user is None:
        abort(403)
    orig = origin(request)
    oct = request.headers.get('content-type')
    if htmlize and (oct is None or oct == 'text/plain' or oct == 'application/x-www-form-urlencoded'):
        # Assume plain text ('application/x-www-form-urlencoded' is
        # the default type for curl when using --data, hence its
        # special treatment)
        ct = 'text/html'
        # TODO clean the HTML provided by the user? See https://github.com/tootsuite/mastodon/pull/10629#issuecomment-485927096 for an interesting discussion
        content = '<p>%s</p>' % (data.decode())
        safecontent = content
    else:
        if oct is not None:
            ct = oct
            if oct == 'text/plain':
                content = data.decode()
            else:
                # TODO Mastodon displays it as text. Need to read the
                # ActivityStreams standard. Is it mandatory to use
                # attachments?
                content = base64.b64encode(data).decode() # No binary in JSON, we need to use Base64
        else:
            ct = 'text/plain'
            content = data.decode()
        if ct != 'text/html' and ct != 'text/plain':
            safecontent = "BINARY"
        else:
            safecontent = content
    document = create_toot
    identifier = lastid()
    document['id'] = 'https://%s/activities/%i' % (domainname, identifier)
    document['object']['id'] = 'https://%s/toots/%i' % (domainname, identifier)
    t = time.gmtime(time.time())
    ft = curtime()
    document['object']['published'] = ft
    document['object']['content'] = content
    document['object']['mediaType'] = ct
    if ct.startswith('text/'):
        if len(content) > MAXNOTELENGTH:
            objecttype = "Article"
        else:
            objecttype = "Note"
    elif ct.startswith('image/'):
        objecttype = "Image"
    elif ct.startswith('audio/'):
        objecttype = "Audio"
    elif ct.startswith('video/'):
        objecttype = "Video"
    else:
        objecttype = "Document"
    document['object']['type'] = objecttype
    info("Post \"%s\" from %s, type %s %s" % (safecontent, orig, objecttype, oct))
    # TODO the document's content can be quite large...
    debug("Post %s: %s" % (document['id'], json.dumps(document)))
    addtoot(identifier,document['object']['id'], document['object']['content'],json.dumps(document).encode(),curtime(),my_id) 
    curs.execute("SELECT name FROM Peers")
    for tuple in curs.fetchall():
        peer = tuple[0]
        jdocument = json.dumps(document)
        (header, date, digest) = signatures.sign(key, my_key,
                                                 {'Host':  peer}, "post /inbox", body=jdocument)
        debug("Forward toot %s to peer %s" % (identifier, peer))
        # Build request
        peer_url = "https://" + peer + "/inbox"
        # TODO Pleroma returns Exception 500 ({"errors":{"detail":"Internal server error"}}). See bug #2.
        status, response = await urlpost(peer_url, headers={'Date': date, 'Signature': header, 'Digest': digest},
                                         data=jdocument.encode())

        debug("Received %s in reply to my POST" % response)
        if status[0] != '2':
           warning("Exception %s (%s)" % (status, response)) # TODO Put in a queue and retry?
    return Response("#%s Done" % identifier, status=200, mimetype='text/plain')

# ActivityPub's client-to-server protocol (section 6 of the standard)
# uses a different system, with the user's outbox. But it requires the
# client to implement a lot of ActivityPub, and authentification (HTTP
# signatures). Our solution is for lighter clients.
@app.route('/api/custom/post', methods=['POST'])	
async def post():
    data = await request.get_data()
    r = await do_post(data, htmlize=True)
    return r

@app.route('/api/custom/postliteral', methods=['POST'])	
async def postliteral():
    data = await request.get_data()
    r = await do_post(data, htmlize=False)
    return r

@app.route('/api/custom/follow', methods=['GET', 'POST'])		
async def follow():
    (authentic, user, resp) = authenticate(request.headers.get('authorization'), mandatory=True)
    if not authentic or user is None:
        abort(403)
    orig = origin(request)
    if request.method == 'POST':
        data = await request.get_data()
        remote_address = data.decode()
    elif request.method == 'HEAD' or request.method == 'GET':
        form = request.args
        if "address" not in form: 
            return Response("Missing address", status=400, mimetype='text/plain')
        remote_address = str(form['address'])
    # TODO already partially done in /inbox. Refactor it?
    try:
        (remote_user, remote_domain) = remote_address.split('@')
        if not remote_user or not remote_domain:
            raise ValueError
    except ValueError:
        return Response("Failed: %s does not look like a fediverse address" % remote_address)
    status, remote_friend = await urlget("https://" + remote_domain + "/.well-known/webfinger?resource=acct:" + remote_address, headers = {'Accept': 'application/jrd+json'})
    if status[0] != '2':
        return Response("Cannot reach %s for verification, status %s, body \"%s\"" % (remote_domain, status, remote_friend), status=502, mimetype='text/plain') 
        # TODO what is returned by WebFinger if remote user does not exist: 404 or 410. Take it as a special case?
    remote_friend = json.loads(remote_friend)
    links = remote_friend['links']
    remote_url = None
    for link in links:
        if link['rel'] == 'self' and link['type'] == 'application/activity+json':
            remote_url = link['href']
            break
    if not remote_url:
        return Response("Cannot get actor URL at %s" % remote_domain, status=502, mimetype='text/plain') 
    status, remote_actor = await urlget(remote_url, headers = {'Accept': 'application/activity+json'})
    if status[0] != '2':
        return Response("Cannot reach %s for actor info" % remote_domain, status=502, mimetype='text/plain') # TODO more detailed error
    remote_actor = json.loads(remote_actor)
    #remote_inbox = remote_actor['inbox'] # TODO Apparently, Mastodon
    #requires we send it to the shared inbox? But other servers may
    #decide otherwise (WriteFreely has no sharedInbox). See also
    #https://www.w3.org/TR/activitypub/#shared-inbox-delivery
    if 'endpoints' in remote_actor and 'sharedInbox' in remote_actor['endpoints']:
        remote_inbox = remote_actor['endpoints']['sharedInbox']
    else:
        remote_inbox = remote_actor['inbox']
    # Tests PeerTube: most (all?) instances are closed https://mastodon.gougere.fr/@bortzmeyer/101551134128700262
    # TONOLA - ERROR - 2019-04-21 18:28:31Z - Reply to our Follow activity from peertube.tamanoir.foucry.net was 403 (Forbidden) (but it accepts follow from Mastodon)
    # TODO Pixelfed (pix.diaspodon.fr) and Funkwhale (funkwhale.mastodon.host) reply 500 after retrieving /actor/username. Unexpected things in it?
    info("Follow %s (%s via %s) from %s" % (remote_address, remote_url, remote_inbox, orig))
    identifier = generator.randint(0,MAXTOOT)
    follow_request['id'] = 'https://%s/requests/%i' % (domainname, identifier)
    follow_request['object'] = remote_url
    document = json.dumps(follow_request)
    debug("Sending %s to %s" % (document, remote_domain))
    (sig_header, date, digest) = signatures.sign(key, my_key,
                                                 {'Host':  remote_domain}, target="post /inbox", body=document)
    status, result = await urlpost(remote_inbox, headers = {'Accept':
                                                            'application/activity+json', 'Host': remote_domain, 'Date': date,
                                                            'Signature': sig_header, 'Digest': digest, 'Content-type':
                                                            'application/activity+json'}, # ActivityPub
                                   # standard says we should use ld+json instead. But Mastodon
                                   # apparently uses this one.
                                   data = document.encode())
    # Pleroma apparently returns 200 even if the body is just
    # "error"??? Better now? Pleroma replied with error 500
    # {"errors":{"detail":"Internal server error"}} when we send our
    # accept (and therefore, subscription from Pleroma stays pending, see pleroma-follow-bug.txt) See bug #2.
    if status[0] != '2':
        error("Reply to our Follow activity from %s was %s (%s)" % (remote_domain, status, result))
        return Response("Cannot send follow request to %s: %s (%s)" % (remote_domain, status, result), status=502, mimetype='text/plain')
            
    debug("Reply to my request to follow from %s: %s (%s)" % (remote_inbox, status, result))
    remotekey = remote_actor['publicKey']['publicKeyPem']
    remoteuri = remote_actor['id']
    if  fetchfollowinguri(remoteuri) is None:
        curs.execute('''INSERT INTO Following (address, uri, key, state, date) VALUES (?,?,?,'PENDING',?)''', (remote_address, remoteuri , remotekey , curtime()))
        bdd.commit()
        return Response("Request to follow %s sent (return status is %s)" % (remote_address, status), status=200, mimetype='text/plain')
    else:
        return Response("Already following %s" % (remote_address),mimetype='text/plain') 

# Main program

# Default .ini file
try:
              with open(configfilename):pass
              cfg.read(configfilename)
              cf = cfg[default_section]
              __name__ = cf.get('name')
              __version__ = cf.get('version')
              robotstxt = cf.get('robots', robotstxt)
              username = cf.get('username')
              password = cf.get('password', None)
              port = cf.get('port')
              max_toots = cf.get('max_toots', max_toots) # TODO several sections in the config file, for presentation, server, etc
              domainname = cf.get('domain')
              dbname = cf.get('database')
              databasename = username + "-" + dbname
              logfilename =  cf.get('logfile')
              user_agent = '%s/%s' % (__name__, __version__)
except IOError:
                debug('no default .ini file \"%s\"' % configfilename)


# Getopt
try:
    optlist, args = getopt.getopt (sys.argv[1:], "hqvu:p:d:l:c:D:",
                                   ["help", "quiet", "verbose", "username", "port=","domain=","log-filename=","cfg-filename=","databasename="])
    for option, value in optlist:
        if option == "--help" or option == "-h":
            usage()
            sys.exit(0)
        elif option == "--verbose" or option == "-v":
            verbose = True
        elif option == "--quiet" or option == "-q":
            verbose = False
        elif option == "cfg" or option == "-c":
            configfilename = value
            try:
              with open(configfilename):pass
              cfg.read(configfilename)
              cf = cfg[default_section]
              __name__ = cf.get('name')
              __version__ = cf.get('version')
              robotstxt = cf.get('robots', robotstxt)
              username = cf.get('username')
              password = cf.get('password', None)
              port = cf.get('port')
              max_toots = cf.get('max_toots', max_toots) # TODO several sections in the config file, for presentation, server, etc
              domainname = cf.get('domain')
              dbname = cf.get('database')
              databasename = username + "-" + dbname
              logfilename =  cf.get('logfile')
              user_agent = '%s/%s' % (__name__, __version__)
            except IOError:
                usage('Configuration file \"%s\" does not exist'% configfilename)
                sys.exit(1)
        elif option == "--user" or option == "-u":
            username = value
            databasename = username + "-" + dbname
        elif option ==  "port" or option == "-p":
            port = value
        elif option == "domain" or option == "-d":
            domainname = value
        elif option == "log" or option == "-l":
            logfilename = value
        elif option == "db" or option == "-D":
           databasename = value 
        else:
            error ("Unknown option " + option)
except getopt.error as reason:
    usage(reason)
    sys.exit(1)
if len(args) != 0:
    usage("Wrong number of arguments (got %i, should be zero)" % len(args))
    sys.exit(1)

my_id = 'https://%s/actors/%s' % (domainname, username)
my_key = my_id + '#main-key'
my_inbox = 'https://%s/inbox' % domainname # Shared inbox, for all
                                           # usernames.  Be careful,
                                           # Mastodon uses two
                                           # inboxes, a shared one and
                                           # a user one. But since
                                           # Tonola is currently
                                           # mono-user, this may be
                                           # not important for us.

# For signatures (read it in the config file instead?).
# Generate the keys with:
#     openssl genrsa -out private-${TONOLAUSER}.pem 2048
#     openssl rsa -in private-${TONOLAUSER}.pem -outform PEM -pubout -out public-${TONOLAUSER}.pem
st_key = open('private-%s.pem' % username, 'rt').read()
key = OpenSSL.crypto.load_privatekey(OpenSSL.crypto.FILETYPE_PEM, st_key)
public_key_text = open('public-%s.pem' % username, 'rt').read()
public_key = OpenSSL.crypto.load_publickey(OpenSSL.crypto.FILETYPE_PEM, public_key_text)

# Logging
logger = logging.getLogger('tonola')
logger.setLevel(logging.DEBUG)
loggingstr = "/tmp/" + logfilename
fh = logging.FileHandler(loggingstr)
ft = logging.Formatter(fmt = 'TONOLA - %(levelname)s - %(asctime)s - %(message)s',
                       datefmt = '%Y-%m-%d %H:%M:%SZ')
ft.converter = time.gmtime
fh.setFormatter(ft)
logger.addHandler(fh)

# Database
bdd = sqlite3.connect(databasename)
curs = bdd.cursor()
creationtable()

adduser("%s@%s" % (username, domainname), my_id, username, username)
addkey(my_key, public_key_text, my_id)

# JSON snippets

webfinger_answer = {"subject": "acct:%s@%s" % (username, domainname),
        "links": [
                {
                        "rel": "self",
                        "type": "application/activity+json",
                        "href": my_id,
                }
        ]
}

# TODO add context "https://w3id.org/security/v1 to all requests that
# need to be signed

actor_answer = {
    "@context": [
        "https://www.w3.org/ns/activitystreams",
        "https://w3id.org/security/v1"],
    "id": my_id,
    "type": "Person", # Allow other types such as Application for a
    # bot. (Mastodon allows bots to self-tag as bots, but I don't
    # think it serves a different type in that case.)
    "preferredUsername": username,
    # The list of mandatory fields is at https://www.w3.org/TR/activitypub/#actor-objects
    "inbox": "https://%s/inbox" % domainname,
    "outbox": "https://%s/outbox" % domainname,
    "followers": "https://%s/%s/followers" % (domainname, username),
    "following": "https://%s/%s/following" % (domainname, username),
    "liked": "https://%s/%s/liked" % (domainname, username),
    "publicKey": {
        "id": my_key,
        "owner": my_id,
        "publicKeyPem": public_key_text
    },
    # Not mandatory, in theory
    "name": username,
    "summary": "Yet another random Tonola account"
    
}

follow_request = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "PLACEHOLDER",
        "type": "Follow",
        "actor": my_id,
        "object": "PLACEHOLDER"
}

accept_message = {
   "@context": "https://www.w3.org/ns/activitystreams",
   "id": "%s#accepts/follows/" % my_id,
   "type": "Accept",
   "actor": my_id,
   "object": {
      "id": "PLACEHOLDER",
      "type": "Follow",
      "actor": "PLACEHOLDER"
   },
   "signature": {
      "type": "RsaSignature2017",
      "creator": my_key,
      "created": "PLACEHOLDER",
      "signatureValue": "PLACEHOLDER"
  }
}

create_toot = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://%s/activities/PLACEHOLDER" % domainname, 
        "type": "Create",
        "actor": my_id,

        "object": {
                "id": "https://%s/toots/PLACEHOLDER" % domainname, 
                "type": "Note",
                "mediaType": "text/html", # https://www.w3.org/TR/activitystreams-vocabulary/#dfn-mediatype
                "published": "PLACEHOLDER",
                "attributedTo": my_id,
                "content": "<p>PLACEHOLDER</p>",
                "to": "https://www.w3.org/ns/activitystreams#Public"
        }
}

check()

generator = random.Random()

# Web footer
doc, tag, text = Doc().tagtext()                                                             
with tag('footer'):
    with tag('hr'):
        pass
    with tag('a', href=__URL__):
        text("%s / %s" % (__name__, __version__))     
footer = doc.getvalue()

# https://lxml.de/api/lxml.html.clean.Cleaner-class.html
html_cleaner = Cleaner(scripts=True, javascript=True, embedded=True,
                       meta=True, page_structure=True, links=True,
                       remove_unknown_tags=True,
                       frames=True,annoying_tags=True,add_nofollow=True,
                       style=False)
# See https://mastodon.social/@Gargron/102125096955948360 for a survey
# about what elements to keep. Note that the ActivityStreams standard
# is silent here. Some people claim that we should keep only "the Atom
# profile" (which is not specified in RFC 4287, but see its section 8.1).

info("Tonola starting, port %s" % port)

app.run(host=me, port=port)

