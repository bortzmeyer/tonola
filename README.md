# Tonola

A very simple ActivityPub (plus related protocols, WebFinger, HTTP
Signatures and some others) server. Intended for experimentation, not
for actual use. *Do not use it for serious work.*

## Goal

Mostly learning the techniques of the federation (collectively called
ActivitySuite, a term coined by Eugen Rochko. "ActivityPub" is often
used alone but, actually, you need more protocols to federate).

TODO: ActivitySuite seems already in used by something else, find
another name https://www.activitysuite.com/

Tonola is *not* intended for production use, reliability, security,
privacy or ease of use were not considered.

Tonola is mono-user and this is a feature, not a bug.

## Usage

Once installed, you can interact with Tonola in two ways:

* via the Web interface
* via the API

If the server is installed at `https://tonola.example.org/` you can
use a regular Web browser to see what happens. Some of the information
is private, you set up a password in the configuration file.

### API

You can use any HTTP client like curl. Authentication is basic HTTP
authentication, with the user and password contained in the
configuration file. (Option `--user` with curl.)

* `/api/custom/inspect/keys`: retrieve the cryptographic keys known by the instance
* `/api/custom/inspect/following`: retrieve the people we follow
* `/api/custom/inspect/followers`: retrieve the people who follow us
* `/api/custom/inspect/peers`: retrieve the fediverse peers (instances
  we send toots to)
* `/api/custom/post`: write a toot. Example with curl: `curl --data 'Test from Tonola' --user foobar:VERYSECRET  https://tonola.example.org/api/custom/post` (plain text is converted to HTML)
* `/api/custom/postliteral`: write a toot without any conversion to HTML (the default format). Example with curl: `curl --data 'Test from Tonola' --header 'Content-Type: text/plain' --user foobar:VERYSECRET  https://tonola.example.org/api/custom/postliteral`
* `/api/custom/follow`: follow someone. The address is either in the
  address parameter (GET) or in the body (POST). Example with curl:
  `curl --data 'eff@mastodon.social' --user foobar:VERYSECRET https://tonola.example.org/api/custom/follow`

## Origin

Tonola de Magnasco was the mother of Francisco de Tassis / Franz von
Taxis who was the founder of the first european postal service, around
1490.

## Installation

You need Python 3 and the following Python packages:

* [Quart](https://gitlab.com/pgjones/quart)
* [Yattag](http://www.yattag.org/)
* [aoihttp](https://docs.aiohttp.org/)
* [OpenSSL](https://pyopenssl.org/)
* [lxml](https://lxml.de/)

There is currently no way to install them automatically (no `setup.py`).

Then, you need to create a configuration file for the local user. The
simplest solution is to start from the sample `config.ini`. You need
to create cryptographic keys for the user. A possible way:
```
export TONOLAUSER=foobar
openssl genrsa -out private-${TONOLAUSER}.pem 2048                                                              
openssl rsa -in private-${TONOLAUSER}.pem -outform PEM -pubout -out public-${TONOLAUSER}.pem 
```

(You can use the script `new-identity.sh` for that.)

The fediverse requires HTTPS, with a certificate by a known Certificate
Authority. Tonola does not implement the ACME protocol used by
certificate authorities like Let's Encrypt. So, the simplest solution
is to use a frontend as a proxy, for instance Apache or nginx. Here is
a possible configuration for Apache:

```
<VirtualHost *:443>
	ServerName tonola.example.org

	DocumentRoot /var/www/tonola.example.org

    ProxyRequests off
    # This is for Let's Encrypt
    ProxyPass /.well-known/acme-challenge !
    # For everything else, proxy it:
    ProxyPass / http://TONOLA.SERVER:5678/
    ProxyPassReverse / http://localhost:5678/
    ProxyPreserveHost On
```

Then, start the Tonola server:

```
./server.py -c foobar.ini
```

## Implementation

Tonola is written in Python. Code is [at Framagit](https://framagit.org/bortzmeyer/tonola). 

Important URIs:

* `/actors/USERNAME`: the user (actor, in ActivityPub parlance). With `#main-key`, you identify the key
* `/toots/1234566`: a toot, as identified by its local ID

Tonola stores everything in a SQLite database, which you can access
directly if you wish.

## Similar stuff

[This page](https://github.com/Shleeble/Big-List-of-ActivityPub) lists
many ActivityPub servers, including some which are in Python, like
Tonola.

## Maintainer

Stéphane Bortzmeyer <bortzmeyer@nic.fr>

With code from Chloé Baut.
