#!/bin/sh

user="$1"
if [ -z "$user" ]; then
    echo "Usage: $0 username" >&2
    exit 1
fi
openssl genrsa -out private-${user}.pem 2048
openssl rsa -in private-${user}.pem -outform PEM -pubout -out public-${user}.pem
sed "s/USERNAME/${user}/g" template.ini > ${user}.ini
echo "Now start Tonola with ./server.py -c ${user}.ini"
