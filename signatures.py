#!/usr/bin/env python3

import time
import base64
import hashlib
import re

# https://pyopenssl.org/
import OpenSSL.crypto

# Not yet configurable
DIGEST_ALGO = "SHA-256"

def sign(private_key, keyID, headers={}, target=None, body=None, sign_digest=True,
         date=None, algo="rsa", hash_algo="sha256"):
    """Returns a tuple of strings (Signature_header, Date_header,
       Digest_header). private_key must have been created with
       OpenSSL.crypto.load_privatekey. target and body must be str. If
       no body is provided, Digest_header is None. If no date is
       provided, we use the current date. DO NOT PROVIDE A DATE IF YOU
       DON'T KNOW WHAT YOU ARE DOING.

    """
    if date is None:
        ft = time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime(time.time()))
    else:
        ft = date
    if target is not None:
        signed_string = "(request-target): %s\n" % target.lower()
        extra_headers = "(request-target)"
    else:
        signed_string = ""
        extra_headers = ""
    for header in headers:
        signed_string += header.lower() + ": " + str(headers[header]) + "\n"
        extra_headers += " %s" % header.lower()
    signed_string += "date: %s" % ft
    if extra_headers != "":
        extra_headers += " date"
    digest_header = None
    if body is not None:
        if DIGEST_ALGO != "SHA-256":
            raise Exception("Internal error, digest algorithm %s is not supported" % DIGEST_ALGO)
        hasher = hashlib.sha256()
        hasher.update(body.encode())
        digest = hasher.digest()
        digest_header = "%s=%s" % (DIGEST_ALGO,  base64.standard_b64encode(digest).decode())
        if sign_digest:
            signed_string += "\ndigest: %s" % digest_header
            if extra_headers != "":
                extra_headers += " digest"
            else:
                extra_headers = "digest"
    signed = OpenSSL.crypto.sign(private_key, signed_string.encode(), hash_algo)
    d_base64 = base64.standard_b64encode(signed).decode()
    sig_header = 'keyId="%s",algorithm="%s-%s",headers="%s",signature="%s"' % \
                 (keyID, algo, hash_algo, extra_headers, d_base64)
    return (sig_header, ft, digest_header)

def verify(public_key, signature, headers_values, target=None, body=None, algo="rsa", hash_algo="sha256"):
    """We return a boolean indicating success or failure and a string
    carrying a message. headers_values is a dict. signature is the
    full signature field, starting with keyID.
    """
    cert = OpenSSL.crypto.X509()
    cert.set_pubkey(public_key)
    sig_fields = {}
    for k, v, terminator in re.findall(r'([a-zA-Z]+)=\"(.+?)\"(,|$)', signature):
        sig_fields[k] = v
    key_id = sig_fields['keyId']
    s_signature = sig_fields['signature']
    if 'headers' in sig_fields:
        headers = sig_fields['headers'].split(' ')
    else:
        headers = ['date']
    cmp_str = ""
    first = True
    headers_values = {k.lower(): v for k, v in headers_values.items()}
    for header in headers:
        if not first:
            cmp_str += '\n'
        else:
            first = False
        if header == "(request-target)":
            if target is not None:
                value = target
            else:
                value = ""
        elif header == "digest":
            if body is None:
                pass # Do not check the digest
            else:
               hasher = hashlib.sha256()
               hasher.update(body.encode())
               digest = "SHA-256=" + base64.standard_b64encode(hasher.digest()).decode()
               if digest != headers_values['digest']:
                   return (False, "wrong digest (%s) for body (should be %s)" % (digest, headers_values['digest']))
               value = headers_values['digest']
        else:
            value = headers_values[header] # return False if KeyError?
        cmp_str += "%s: %s" % (header, value)
    try:
        verif = OpenSSL.crypto.verify(cert, base64.standard_b64decode(s_signature),
                                      cmp_str.encode(), hash_algo)
        if verif == None:
            return (True, "")
        else:
            return (False, "Signature verification failed (corrupted data?)")
    except OpenSSL.crypto.Error as e:
        # Syntax error in the key (may be truncated accidentally)
        return (False, "Signature verification exception %s" % e)
        
if __name__ == '__main__':

    # Only for tests
    PRIVATE_KEY = 'private.pem'
    PUBLIC_KEY = 'public.pem'
    RFC_PRIVATE_KEY = 'rfc-private.pem'
    RFC_PUBLIC_KEY = 'rfc-public.pem'

    # Create keys with:
    # openssl genrsa -out private.pem 2048
    # openssl rsa -in private.pem -outform PEM -pubout -out public.pem
    private_key = open(PRIVATE_KEY, 'rt').read()
    key = OpenSSL.crypto.load_privatekey(OpenSSL.crypto.FILETYPE_PEM, private_key)
    print(sign(key, "file:///%s" % PRIVATE_KEY, {}, "POST /inbox"))
    print(sign(key, "file:///%s" % PRIVATE_KEY, {'Foo': 'Bar', 'Baz': 42}, "POST /inbox"))
    print(sign(key, "file:///%s" % PRIVATE_KEY, target="POST /inbox", body='Hello, world'))

    print("")
    # RFC keys
    rfc_private_key = open(RFC_PRIVATE_KEY, 'rt').read()
    key = OpenSSL.crypto.load_privatekey(OpenSSL.crypto.FILETYPE_PEM, rfc_private_key)
    # No target (default test, C1)
    print(sign(key, "Test", date='Sun, 05 Jan 2014 21:31:40 GMT'))
    # (basic test, C2)
    print(sign(key, "Test", sign_digest=False, target="post /foo?param=value&pet=dog",
               headers={'Host': 'example.com'},
               body='{"hello": "world"}', date='Sun, 05 Jan 2014 21:31:40 GMT'))
    # With the body (all headers test, C3) not done because headers are in a different order

    print("")
    public_key = open(PUBLIC_KEY, 'rt').read()
    s_key = OpenSSL.crypto.load_privatekey(OpenSSL.crypto.FILETYPE_PEM, private_key)
    v_key = OpenSSL.crypto.load_publickey(OpenSSL.crypto.FILETYPE_PEM, public_key)
    print("Signature is %s" % \
          verify(v_key, sign(s_key, "file:///%s" % PRIVATE_KEY,
                             {'Foo': 'Bar', 'Baz': 42}, date = "Mon, 28 Jan 2019 12:41:52 GMT",
                             target = "POST /inbox")[0],
                 headers_values={'Foo': 'Bar', 'Baz': 42, 'Date': "Mon, 28 Jan 2019 12:41:52 GMT"},
                 target="post /inbox"))
    print("Signature is %s" % \
          verify(v_key, sign(s_key, "file:///%s" % PRIVATE_KEY,
                             date = "Mon, 28 Jan 2019 12:41:52 GMT",
                             target = "POST /inbox")[0],
                 headers_values={'Date': "Mon, 28 Jan 2019 12:41:52 GMT"},
                 target="post /inbox"))
    rfc_public_key = open(RFC_PUBLIC_KEY, 'rt').read()
    key = OpenSSL.crypto.load_publickey(OpenSSL.crypto.FILETYPE_PEM, rfc_public_key)
    print("Signature is %s" % \
          verify(key, 'keyId="Test",algorithm="rsa-sha256",signature="SjWJWbWN7i0wzBvtPl8rbASWz5xQW6mcJmn+ibttBqtifLN7Sazz6m79cNfwwb8DMJ5cou1s7uEGKKCs+FLEEaDV5lp7q25WqS+lavg7T8hc0GppauB6hbgEKTwblDHYGEtbGmtdHgVCk9SuS13F0hZ8FD0k/5OxEPXe5WozsbM="',
                 {'Date': "Sun, 05 Jan 2014 21:31:40 GMT"}))
    print("Signature is %s" % \
          verify(key, 'keyId="Test",algorithm="rsa-sha256",headers="(request-target) host date",signature="qdx+H7PHHDZgy4y/Ahn9Tny9V3GP6YgBPyUXMmoxWtLbHpUnXS2mg2+SbrQDMCJypxBLSPQR2aAjn7ndmw2iicw3HMbe8VfEdKFYRqzic+efkb3nndiv/x1xSHDJWeSWkx3ButlYSuBskLu6kd9Fswtemr3lgdDEmn04swr2Os0="',
                 {'Date': "Sun, 05 Jan 2014 21:31:40 GMT", 'Host': 'example.com'},
                 target="post /foo?param=value&pet=dog"))
